provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "default" {
  name     = var.resource_group
  location = var.location

  tags = {
    environment = "Demo"
  }
}

resource "azurerm_kubernetes_cluster" "default" {
  name                = join("-",[var.stack,"aks"])
  location            = azurerm_resource_group.default.location
  resource_group_name = azurerm_resource_group.default.name
  dns_prefix          = join("-",[var.stack,"k8s"])

  default_node_pool {
    name            = "default"
    node_count      = var.node_count
    vm_size         = var.node_size
    os_disk_size_gb = 30
  }

  service_principal {
    client_id     = var.client_id
    client_secret = var.client_secret
  }

  addon_profile {
      oms_agent {
        enabled = false
      }
  }

  role_based_access_control {
    enabled = true
  }

  tags = {
    stack = var.stack
  }
}

resource "local_file" "kubeconfig" {
    content  = azurerm_kubernetes_cluster.default.kube_config_raw
    filename = "kubeconfig.yml"
}